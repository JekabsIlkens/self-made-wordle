function jumpOne(elmnt, content){

    if (content.length == elmnt.maxLength){
      
        next = elmnt.tabIndex      
        if (next < document.forms[0].elements.length){ document.forms[0].elements[next].focus() }
    }
}

function jumpTwo(elmnt, content){
    
    if (content.length == elmnt.maxLength){
      
        next = elmnt.tabIndex     
        if (next < document.forms[1].elements.length){ document.forms[1].elements[next].focus() }
    }
}

function jumpThree(elmnt, content){
    
    if (content.length == elmnt.maxLength){
      
        next = elmnt.tabIndex    
        if (next < document.forms[2].elements.length){ document.forms[2].elements[next].focus() }
    }
}

function jumpFour(elmnt, content){
    
    if (content.length == elmnt.maxLength){
      
        next = elmnt.tabIndex     
        if (next < document.forms[3].elements.length){ document.forms[3].elements[next].focus() }
    }
}

function jumpFive(elmnt, content){
    
    if (content.length == elmnt.maxLength){
      
        next = elmnt.tabIndex     
        if (next < document.forms[4].elements.length){ document.forms[4].elements[next].focus() }
    }
}

function jumpSix(elmnt, content){
    
    if (content.length == elmnt.maxLength){
      
        next = elmnt.tabIndex     
        if (next < document.forms[5].elements.length){ document.forms[5].elements[next].focus() }
    }
}

let secret = "ADATA";



function rowOne(){
        
    let letter1 = document.getElementById("row1letter1").value;
    let letter2 = document.getElementById("row1letter2").value;
    let letter3 = document.getElementById("row1letter3").value;
    let letter4 = document.getElementById("row1letter4").value;
    let letter5 = document.getElementById("row1letter5").value;

    letter1 = letter1.toUpperCase();
    letter2 = letter2.toUpperCase();
    letter3 = letter3.toUpperCase();
    letter4 = letter4.toUpperCase();
    letter5 = letter5.toUpperCase();

    if(secret.includes(letter1, 0)){ 
        let index = secret.indexOf(letter1);
        if(index == 0){ 
            document.getElementById("row1letter1").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter1).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row1letter1").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter1).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row1letter1").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter1).style.backgroundColor = "#9a9a9a";
    }

    if(secret.includes(letter2, 0)){ 
        let index = secret.indexOf(letter2);
        if(index == 1){ 
            document.getElementById("row1letter2").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter2).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row1letter2").style.backgroundColor = "#cc474a";
            document.getElementById(letter2).style.backgroundColor = "#cc474a"; 
        } 
    }
    else{ 
        document.getElementById("row1letter2").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter2).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter3, 0)){ 
        let index = secret.indexOf(letter3);
        if(index == 2){ 
            document.getElementById("row1letter3").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter3).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row1letter3").style.backgroundColor = "#cc474a";
            document.getElementById(letter3).style.backgroundColor = "#cc474a"; 
        } 
    }  
    else{ 
        document.getElementById("row1letter3").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter3).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter4, 0)){ 
        let index = secret.indexOf(letter4);
        if(index == 3){ 
            document.getElementById("row1letter4").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter4).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row1letter4").style.backgroundColor = "#cc474a";
            document.getElementById(letter4).style.backgroundColor = "#cc474a"; 
        }
    }
    else{ 
        document.getElementById("row1letter4").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter4).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter5, 0)){ 
        let index = secret.indexOf(letter5);
        if(index == 4){ 
            document.getElementById("row1letter5").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter5).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row1letter5").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter5).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row1letter5").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter5).style.backgroundColor = "#9a9a9a";
    }

    if(secret[0] == letter1 && secret[1] == letter2 && secret[2] == letter3 && secret[3] == letter4 && secret[4] == letter5){
        document.getElementById("row1letter1").style.backgroundColor = "#437a4e";
        document.getElementById("row1letter2").style.backgroundColor = "#437a4e";
        document.getElementById("row1letter3").style.backgroundColor = "#437a4e";
        document.getElementById("row1letter4").style.backgroundColor = "#437a4e";
        document.getElementById("row1letter5").style.backgroundColor = "#437a4e";
        document.getElementById("overlay").style.display = "block";
    }

    document.getElementById("row2letter1").focus();
}

function rowTwo(){
        
    let letter1 = document.getElementById("row2letter1").value;
    let letter2 = document.getElementById("row2letter2").value;
    let letter3 = document.getElementById("row2letter3").value;
    let letter4 = document.getElementById("row2letter4").value;
    let letter5 = document.getElementById("row2letter5").value;

    letter1 = letter1.toUpperCase();
    letter2 = letter2.toUpperCase();
    letter3 = letter3.toUpperCase();
    letter4 = letter4.toUpperCase();
    letter5 = letter5.toUpperCase();

    if(secret.includes(letter1, 0)){ 
        let index = secret.indexOf(letter1);
        if(index == 0){ 
            document.getElementById("row2letter1").style.backgroundColor = "#437a4e";
            document.getElementById(letter1).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row2letter1").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter1).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row2letter1").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter1).style.backgroundColor = "#9a9a9a";
    }

    if(secret.includes(letter2, 0)){ 
        let index = secret.indexOf(letter2);
        if(index == 1){ 
            document.getElementById("row2letter2").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter2).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row2letter2").style.backgroundColor = "#cc474a";
            document.getElementById(letter2).style.backgroundColor = "#cc474a"; 
        } 
    }
    else{ 
        document.getElementById("row2letter2").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter2).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter3, 0)){ 
        let index = secret.indexOf(letter3);
        if(index == 2){ 
            document.getElementById("row2letter3").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter3).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row2letter3").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter3).style.backgroundColor = "#cc474a";
        } 
    }  
    else{ 
        document.getElementById("row2letter3").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter3).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter4, 0)){ 
        let index = secret.indexOf(letter4);
        if(index == 3){ 
            document.getElementById("row2letter4").style.backgroundColor = "#437a4e";
            document.getElementById(letter4).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row2letter4").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter4).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row2letter4").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter4).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter5, 0)){ 
        let index = secret.indexOf(letter5);
        if(index == 4){ 
            document.getElementById("row2letter5").style.backgroundColor = "#437a4e";
            document.getElementById(letter5).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row2letter5").style.backgroundColor = "#cc474a";
            document.getElementById(letter5).style.backgroundColor = "#cc474a"; 
        }
    }
    else{ 
        document.getElementById("row2letter5").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter5).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret[0] == letter1 && secret[1] == letter2 && secret[2] == letter3 && secret[3] == letter4 && secret[4] == letter5){
        document.getElementById("row2letter1").style.backgroundColor = "#437a4e";
        document.getElementById("row2letter2").style.backgroundColor = "#437a4e";
        document.getElementById("row2letter3").style.backgroundColor = "#437a4e";
        document.getElementById("row2letter4").style.backgroundColor = "#437a4e";
        document.getElementById("row2letter5").style.backgroundColor = "#437a4e";
        document.getElementById("overlay").style.display = "block";
    }

    document.getElementById("row3letter1").focus();
}

function rowThree(){
        
    let letter1 = document.getElementById("row3letter1").value;
    let letter2 = document.getElementById("row3letter2").value;
    let letter3 = document.getElementById("row3letter3").value;
    let letter4 = document.getElementById("row3letter4").value;
    let letter5 = document.getElementById("row3letter5").value;

    letter1 = letter1.toUpperCase();
    letter2 = letter2.toUpperCase();
    letter3 = letter3.toUpperCase();
    letter4 = letter4.toUpperCase();
    letter5 = letter5.toUpperCase();

    if(secret.includes(letter1, 0)){ 
        let index = secret.indexOf(letter1);
        if(index == 0){ 
            document.getElementById("row3letter1").style.backgroundColor = "#437a4e";
            document.getElementById(letter1).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row3letter1").style.backgroundColor = "#cc474a";
            document.getElementById(letter1).style.backgroundColor = "#cc474a"; 
        }
    }
    else{ 
        document.getElementById("row3letter1").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter1).style.backgroundColor = "#9a9a9a";
    }

    if(secret.includes(letter2, 0)){ 
        let index = secret.indexOf(letter2);
        if(index == 1){ 
            document.getElementById("row3letter2").style.backgroundColor = "#437a4e";
            document.getElementById(letter2).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row3letter2").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter2).style.backgroundColor = "#cc474a";
        } 
    }
    else{ 
        document.getElementById("row3letter2").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter2).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter3, 0)){ 
        let index = secret.indexOf(letter3);
        if(index == 2){ 
            document.getElementById("row3letter3").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter3).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row3letter3").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter3).style.backgroundColor = "#cc474a";
        } 
    }  
    else{ 
        document.getElementById("row3letter3").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter3).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter4, 0)){ 
        let index = secret.indexOf(letter4);
        if(index == 3){ 
            document.getElementById("row3letter4").style.backgroundColor = "#437a4e";
            document.getElementById(letter4).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row3letter4").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter4).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row3letter4").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter4).style.backgroundColor = "#9a9a9a";
    }

    if(secret.includes(letter5, 0)){ 
        let index = secret.indexOf(letter5);
        if(index == 4){ 
            document.getElementById("row3letter5").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter5).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row3letter5").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter5).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row3letter5").style.backgroundColor = "#9a9a9a";
        document.getElementById(letter5).style.backgroundColor = "#9a9a9a"; 
    }
    
    if(secret[0] == letter1 && secret[1] == letter2 && secret[2] == letter3 && secret[3] == letter4 && secret[4] == letter5){
        document.getElementById("row3letter1").style.backgroundColor = "#437a4e";
        document.getElementById("row3letter2").style.backgroundColor = "#437a4e";
        document.getElementById("row3letter3").style.backgroundColor = "#437a4e";
        document.getElementById("row3letter4").style.backgroundColor = "#437a4e";
        document.getElementById("row3letter5").style.backgroundColor = "#437a4e";
        document.getElementById("overlay").style.display = "block";
    }

    document.getElementById("row4letter1").focus();
}

function rowFour(){
        
    let letter1 = document.getElementById("row4letter1").value;
    let letter2 = document.getElementById("row4letter2").value;
    let letter3 = document.getElementById("row4letter3").value;
    let letter4 = document.getElementById("row4letter4").value;
    let letter5 = document.getElementById("row4letter5").value;

    letter1 = letter1.toUpperCase();
    letter2 = letter2.toUpperCase();
    letter3 = letter3.toUpperCase();
    letter4 = letter4.toUpperCase();
    letter5 = letter5.toUpperCase();

    if(secret.includes(letter1, 0)){ 
        let index = secret.indexOf(letter1);
        if(index == 0){ 
            document.getElementById("row4letter1").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter1).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row4letter1").style.backgroundColor = "#cc474a";
            document.getElementById(letter1).style.backgroundColor = "#cc474a"; 
        }
    }
    else{ 
        document.getElementById("row4letter1").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter1).style.backgroundColor = "#9a9a9a";
    }

    if(secret.includes(letter2, 0)){ 
        let index = secret.indexOf(letter2);
        if(index == 1){ 
            document.getElementById("row4letter2").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter2).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row4letter2").style.backgroundColor = "#cc474a";
            document.getElementById(letter2).style.backgroundColor = "#cc474a"; 
        } 
    }
    else{ 
        document.getElementById("row4letter2").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter2).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter3, 0)){ 
        let index = secret.indexOf(letter3);
        if(index == 2){ 
            document.getElementById("row4letter3").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter3).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row4letter3").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter3).style.backgroundColor = "#cc474a";
        } 
    }  
    else{ 
        document.getElementById("row4letter3").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter3).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter4, 0)){ 
        let index = secret.indexOf(letter4);
        if(index == 3){ 
            document.getElementById("row4letter4").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter4).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row4letter4").style.backgroundColor = "#cc474a";
            document.getElementById(letter4).style.backgroundColor = "#cc474a"; 
        }
    }
    else{ 
        document.getElementById("row4letter4").style.backgroundColor = "#9a9a9a";
        document.getElementById(letter4).style.backgroundColor = "#9a9a9a"; 
    } 

    if(secret.includes(letter5, 0)){ 
        let index = secret.indexOf(letter5);
        if(index == 4){ 
            document.getElementById("row4letter5").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter5).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row4letter5").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter5).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row4letter5").style.backgroundColor = "#9a9a9a";
        document.getElementById(letter5).style.backgroundColor = "#9a9a9a"; 
    } 
    
    if(secret[0] == letter1 && secret[1] == letter2 && secret[2] == letter3 && secret[3] == letter4 && secret[4] == letter5){
        document.getElementById("row4letter1").style.backgroundColor = "#437a4e";
        document.getElementById("row4letter2").style.backgroundColor = "#437a4e";
        document.getElementById("row4letter3").style.backgroundColor = "#437a4e";
        document.getElementById("row4letter4").style.backgroundColor = "#437a4e";
        document.getElementById("row4letter5").style.backgroundColor = "#437a4e";
        document.getElementById("overlay").style.display = "block";
    }

    document.getElementById("row5letter1").focus();
}

function rowFive(){
        
    let letter1 = document.getElementById("row5letter1").value;
    let letter2 = document.getElementById("row5letter2").value;
    let letter3 = document.getElementById("row5letter3").value;
    let letter4 = document.getElementById("row5letter4").value;
    let letter5 = document.getElementById("row5letter5").value;

    letter1 = letter1.toUpperCase();
    letter2 = letter2.toUpperCase();
    letter3 = letter3.toUpperCase();
    letter4 = letter4.toUpperCase();
    letter5 = letter5.toUpperCase();

    if(secret.includes(letter1, 0)){ 
        let index = secret.indexOf(letter1);
        if(index == 0){ 
            document.getElementById("row5letter1").style.backgroundColor = "#437a4e";
            document.getElementById(letter1).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row5letter1").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter1).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row5letter1").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter1).style.backgroundColor = "#9a9a9a";
    }

    if(secret.includes(letter2, 0)){ 
        let index = secret.indexOf(letter2);
        if(index == 1){ 
            document.getElementById("row5letter2").style.backgroundColor = "#437a4e";
            document.getElementById(letter2).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row5letter2").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter2).style.backgroundColor = "#cc474a";
        } 
    }
    else{ 
        document.getElementById("row5letter2").style.backgroundColor = "#9a9a9a";
        document.getElementById(letter2).style.backgroundColor = "#9a9a9a"; 
    }
    
    if(secret.includes(letter3, 0)){ 
        let index = secret.indexOf(letter3);
        if(index == 2){ 
            document.getElementById("row5letter3").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter3).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row5letter3").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter3).style.backgroundColor = "#cc474a";
        } 
    }  
    else{ 
        document.getElementById("row5letter3").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter3).style.backgroundColor = "#9a9a9a";
    }
    
    if(secret.includes(letter4, 0)){ 
        let index = secret.indexOf(letter4);
        if(index == 3){ 
            document.getElementById("row5letter4").style.backgroundColor = "#437a4e";
            document.getElementById(letter4).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row5letter4").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter4).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row5letter4").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter4).style.backgroundColor = "#9a9a9a";
    } 

    if(secret.includes(letter5, 0)){ 
        let index = secret.indexOf(letter5);
        if(index == 4){ 
            document.getElementById("row5letter5").style.backgroundColor = "#437a4e";
            document.getElementById(letter5).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row5letter5").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter5).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row5letter5").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter5).style.backgroundColor = "#9a9a9a";
    } 
    
    if(secret[0] == letter1 && secret[1] == letter2 && secret[2] == letter3 && secret[3] == letter4 && secret[4] == letter5){
        document.getElementById("row5letter1").style.backgroundColor = "#437a4e";
        document.getElementById("row5letter2").style.backgroundColor = "#437a4e";
        document.getElementById("row5letter3").style.backgroundColor = "#437a4e";
        document.getElementById("row5letter4").style.backgroundColor = "#437a4e";
        document.getElementById("row5letter5").style.backgroundColor = "#437a4e";
        document.getElementById("overlay").style.display = "block";
    }

    document.getElementById("row6letter1").focus();
}

function rowSix(){
        
    let letter1 = document.getElementById("row6letter1").value;
    let letter2 = document.getElementById("row6letter2").value;
    let letter3 = document.getElementById("row6letter3").value;
    let letter4 = document.getElementById("row6letter4").value;
    let letter5 = document.getElementById("row6letter5").value;

    letter1 = letter1.toUpperCase();
    letter2 = letter2.toUpperCase();
    letter3 = letter3.toUpperCase();
    letter4 = letter4.toUpperCase();
    letter5 = letter5.toUpperCase();

    if(secret.includes(letter1, 0)){ 
        let index = secret.indexOf(letter1);
        if(index == 0){ 
            document.getElementById("row6letter1").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter1).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row6letter1").style.backgroundColor = "#cc474a"; 
            document.getElementById(letter1).style.backgroundColor = "#cc474a";
        }
    }
    else{ 
        document.getElementById("row6letter1").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter1).style.backgroundColor = "#9a9a9a";
    }

    if(secret.includes(letter2, 0)){ 
        let index = secret.indexOf(letter2);
        if(index == 1){ 
            document.getElementById("row6letter2").style.backgroundColor = "#437a4e";
            document.getElementById(letter2).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row6letter2").style.backgroundColor = "#cc474a";
            document.getElementById(letter2).style.backgroundColor = "#cc474a";
        } 
    }
    else{ 
        document.getElementById("row6letter2").style.backgroundColor = "#9a9a9a";
        document.getElementById(letter2).style.backgroundColor = "#9a9a9a"; 
    }
    
    if(secret.includes(letter3, 0)){ 
        let index = secret.indexOf(letter3);
        if(index == 2){ 
            document.getElementById("row6letter3").style.backgroundColor = "#437a4e";
            document.getElementById(letter3).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row6letter3").style.backgroundColor = "#cc474a";
            document.getElementById(letter3).style.backgroundColor = "#cc474a"; 
        } 
    }  
    else{ 
        document.getElementById("row6letter3").style.backgroundColor = "#9a9a9a";
        document.getElementById(letter3).style.backgroundColor = "#9a9a9a"; 
    }
    
    if(secret.includes(letter4, 0)){ 
        let index = secret.indexOf(letter4);
        if(index == 3){ 
            document.getElementById("row6letter4").style.backgroundColor = "#437a4e"; 
            document.getElementById(letter4).style.backgroundColor = "#437a4e";
        }
        else{ 
            document.getElementById("row6letter4").style.backgroundColor = "#cc474a";
            document.getElementById(letter4).style.backgroundColor = "#cc474a"; 
        }
    }
    else{ 
        document.getElementById("row6letter4").style.backgroundColor = "#9a9a9a"; 
        document.getElementById(letter4).style.backgroundColor = "#9a9a9a";
    } 

    if(secret.includes(letter5, 0)){ 
        let index = secret.indexOf(letter5);
        if(index == 4){ 
            document.getElementById("row6letter5").style.backgroundColor = "#437a4e";
            document.getElementById(letter5).style.backgroundColor = "#437a4e"; 
        }
        else{ 
            document.getElementById("row6letter5").style.backgroundColor = "#cc474a";
            document.getElementById(letter5).style.backgroundColor = "#cc474a";
             
        }
    }
    else{ 
        document.getElementById("row6letter5").style.backgroundColor = "#9a9a9a";
        document.getElementById(letter5).style.backgroundColor = "#9a9a9a"; 
    } 
    
    if(secret[0] == letter1 && secret[1] == letter2 && secret[2] == letter3 && secret[3] == letter4 && secret[4] == letter5){
        document.getElementById("row6letter1").style.backgroundColor = "#437a4e";
        document.getElementById("row6letter2").style.backgroundColor = "#437a4e";
        document.getElementById("row6letter3").style.backgroundColor = "#437a4e";
        document.getElementById("row6letter4").style.backgroundColor = "#437a4e";
        document.getElementById("row6letter5").style.backgroundColor = "#437a4e";
        document.getElementById("overlay").style.display = "block";
    }

}